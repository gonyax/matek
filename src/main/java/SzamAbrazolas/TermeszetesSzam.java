package SzamAbrazolas;

import java.nio.charset.StandardCharsets;
import java.time.chrono.ThaiBuddhistEra;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TermeszetesSzam implements Szam {
    /*
     *  Nem negatív egész számok (a 0 is)
     */

    protected List<Byte> egesz; // A listában visszafelé szerepel a szám

    public TermeszetesSzam() {
        this.egesz = null;
    }

    public TermeszetesSzam(String egesz) {
        String wEgesz = Szam.wStringCreate(egesz).substring(1);
        Szam.szamValidalas( !isTermeszetesSzam(wEgesz), "A szám nem természetes szám!");
        this.egesz = wEgesz.chars()
                        .mapToObj(x -> (byte)(x-48))
                        .collect(Collectors.toList());
    }

    public static boolean isTermeszetesSzam(String szam) {
        if  ( (szam==null) || (szam.isBlank()) ) {
            return false;
        }
        String wSzam = szam.charAt(0) == '+' ? szam.substring(1) : szam;
        String regex = "[0-9]+";
        return wSzam.matches(regex);
    }

    @Override
    public TermeszetesSzam szamTisztitas() {
        int endi;
        for (endi=0; endi<this.egesz.size(); endi++) {
            if (this.egesz.get(endi)>0) { break; }
        }
        if (endi==0) { return this; }
        if (endi>=this.egesz.size()) {return new TermeszetesSzam("0"); }

        TermeszetesSzam ret = new TermeszetesSzam("0");
        ret.egesz.clear();
        for (int i=endi; i<this.egesz.size(); i++) {
            ret.egesz.add(this.egesz.get(i));
        }
        return ret;
    }

    @Override
    public TermeszetesSzam meg(Szam szam) {
        String osszeg = "";
        byte atvitel = 0;
        TermeszetesSzam tSzam = ((TermeszetesSzam) szam);
        int tagSize = Math.max(tSzam.getEgesz().size(), this.getEgesz().size());
        for (int tag=1; tag<=tagSize; tag++) {
            int x = this.getEgesz().size()-tag<0  ? 0 : this.getEgesz().get(this.getEgesz().size()-tag);
            int y = tSzam.getEgesz().size()-tag<0 ? 0 : tSzam.getEgesz().get(tSzam.getEgesz().size()-tag);
            if ((x+y+atvitel)>9) {
                osszeg = (byte) (x + y + atvitel - 10) + osszeg;
                atvitel = 1;
            } else {
                osszeg = (byte) (x + y + atvitel) + osszeg;
                atvitel = 0;
            }
        }
        osszeg = atvitel + osszeg;
        return new TermeszetesSzam(osszeg).szamTisztitas();
    }

    @Override
    public TermeszetesSzam bol(Szam szam) {
        Szam.szamValidalas(this.nagyobb(szam),"Hibás kivonás");

        String difi = "";
        byte atvitel = 0;
        TermeszetesSzam tSzam = ((TermeszetesSzam) szam);
        int tagSize = Math.max(tSzam.getEgesz().size(), this.getEgesz().size());
        for (int tag=1; tag<=tagSize; tag++) {
            int x = this.getEgesz().size()-tag<0  ? 0 : this.getEgesz().get(this.getEgesz().size()-tag);
            int y = tSzam.getEgesz().size()-tag<0 ? 0 : tSzam.getEgesz().get(tSzam.getEgesz().size()-tag);
            if ((x-y-atvitel)<0) {
                difi = (byte) (x + 10 - y - atvitel) + difi;
                atvitel = 1;
            } else {
                difi = (byte) (x - y - atvitel) + difi;
                atvitel = 0;
            }
       }
        return new TermeszetesSzam(difi).szamTisztitas();
    }

    @Override
    public Szam szorozva(Szam szam) {
        TermeszetesSzam tSzam = ((TermeszetesSzam) szam);
        TermeszetesSzam ret = new TermeszetesSzam("0");

        String sz;
        String helyiertek="";
        int atvitel, x,y, egyes, szorzat;
        for (int tagx=1; tagx<=this.getEgesz().size(); tagx++) {
            x = this.getEgesz().get(this.getEgesz().size()-tagx);
            sz="";
            atvitel=0;
            for (int tagy=1; tagy<=tSzam.getEgesz().size(); tagy++) {
                y = tSzam.getEgesz().get(tSzam.getEgesz().size()-tagy);
                szorzat = x * y;
                egyes   = (szorzat % 10);
                sz = (egyes + atvitel) + sz;
                atvitel = (szorzat / 10);
            }
            sz = (atvitel>0 ? String.valueOf(atvitel) : "") + sz + helyiertek;
            helyiertek += "0";
            ret = ret.meg(new TermeszetesSzam(sz));
        }
        return ret;
    }

    @Override
    public Szam osztva(Szam szam) {
        return null;
    }

    @Override
    public boolean nagyobb(Szam szam){
        List<Byte> l = new ArrayList<>( ((TermeszetesSzam)szam).szamTisztitas().getEgesz() );

        if (l.size() < this.egesz.size()) { return false; }
        if (l.size() > this.egesz.size()) { return true; }

        for (int helyiErtek=0; helyiErtek<this.egesz.size(); helyiErtek++) {
            if (l.get(helyiErtek) < this.egesz.get(helyiErtek)) { return false; }
            if (l.get(helyiErtek) > this.egesz.get(helyiErtek)) { return true; }
        }
        return false;
    }

    @Override
    public boolean kisebb(Szam szam) {
        List<Byte> l = new ArrayList<>( ((TermeszetesSzam)szam).szamTisztitas().getEgesz() );

        if (l.size() > this.egesz.size()) { return false; }
        if (l.size() < this.egesz.size()) { return true; }

        for (int helyiErtek=0; helyiErtek<this.egesz.size(); helyiErtek++) {
            if (l.get(helyiErtek) > this.egesz.get(helyiErtek)) { return false; }
            if (l.get(helyiErtek) < this.egesz.get(helyiErtek)) { return true; }
        }
        return false;
    }

    @Override
    public boolean egyenlo(Szam szam) {
        List<Byte> l = new ArrayList<>( ((TermeszetesSzam)szam).szamTisztitas().getEgesz() );

        if (l.size() != this.egesz.size()) { return false; }

        for (int helyiErtek=0; helyiErtek<this.egesz.size(); helyiErtek++) {
            if (!l.get(helyiErtek).equals(this.egesz.get(helyiErtek))) { return false; }
        }
        return true;
    }

    @Override
    public String toString() {
        return "x= " +
                this.egesz.stream()
                        .map(Object::toString)
                        .collect(Collectors.joining());
    }

    @Override
    public String toSzamString() {
        return this.egesz.stream()
                .map(Object::toString)
                .collect(Collectors.joining());
    }

    public List<Byte> getEgesz() {
        return this.egesz;
    }

    public void setEgesz(List<Byte> egesz) {
        this.egesz = egesz;
    }
}
