package SzamAbrazolas;

public interface Szam {

    public abstract Szam meg(Szam szam);  // A "public abstract"-ot nem kötelező kiírni!
    public abstract Szam bol(Szam szam);
    public abstract Szam szorozva(Szam szam);
    public abstract Szam osztva(Szam szam);
    public abstract boolean nagyobb(Szam szam);
    public abstract boolean kisebb(Szam szam);
    public abstract boolean egyenlo(Szam szam);
    public abstract Szam szamTisztitas(); // pl. vezető nullák törlése
    public abstract String toString();
    public abstract String toSzamString();

    public static String wStringCreate(String szam) {
        szamValidalas( ((szam==null) || (szam.equals("")) ),"A szám nem lehet null vagy üres");
        String elojel;
        String wSzam;
        switch (szam.substring(0,1)) {
            case "+": elojel="+"; wSzam=szam.substring(1); break;
            case "-": elojel="-"; wSzam=szam.substring(1); break;
            default:  elojel="+"; wSzam=szam; break;
        }
        String regex = "[0-9]+";
        szamValidalas( !wSzam.matches(regex), "A szám csak számjegyeket tartalmazhat");
        return elojel+wSzam;
    }

    public static Integer wStringPoz(String szam) {
        return szam.indexOf(",") > 0
                ? szam.indexOf(",")
                : szam.indexOf(".") > 0
                    ? szam.indexOf(".")
                    : -1;
    }

    public static String wStringEgesz(String szam) {
        return wStringPoz(szam)>=0
                ? szam.substring(0,wStringPoz(szam))
                : szam;
    }

    public static void szamValidalas(boolean b, String message) {
        if (b) {
            throw new IllegalArgumentException(message);
        }
    }
}
