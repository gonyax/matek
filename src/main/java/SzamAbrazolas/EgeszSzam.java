package SzamAbrazolas;

import java.util.List;
import java.util.stream.Collectors;

public class EgeszSzam extends TermeszetesSzam implements Szam {
    /*
     *  A negatív és pozitív egész számok és a nulla
     */

    protected Boolean pozitiv;

    public EgeszSzam() { this.pozitiv = null; }

    public EgeszSzam(String egesz) {
        super(Szam.wStringCreate(egesz));
        this.pozitiv = Szam.wStringCreate(egesz).charAt(0)=='+';
    }

    public static boolean isEgeszSzam(String szam) {
        if  ( (szam==null) || (szam.isBlank()) ) {
            return false;
        }

        //String wSzam = szam.substring(0,1).matches("['+','-']")
        String wSzam = "['+','-']".matches(szam.substring(0,1))
                           ? szam.substring(1)
                           : szam;
        String regex = "[0-9]+";
        return wSzam.matches(regex);
    }
    public Boolean getPozitiv() {
        return this.pozitiv;
    }

    TermeszetesSzam tMivel = new TermeszetesSzam();
    TermeszetesSzam tMit = new TermeszetesSzam();

    @Override
    public EgeszSzam meg(Szam szam) {
        tMivel.setEgesz( ((TermeszetesSzam)szam).getEgesz() );
        tMit.setEgesz( this.getEgesz() );

        return new EgeszSzam(
/*azonos előjelű*/
                this.getPozitiv()==((EgeszSzam)szam).getPozitiv()
                ? (this.getPozitiv()?"+":"-")+ tMit.meg(tMivel).toSzamString()
/*ellentétes előjelű*/
                : tMit.egyenlo(tMivel) ? "0"
                : tMit.kisebb(tMivel) ? (this.getPozitiv()?"+":"-") + tMit.bol(tMivel).toSzamString()
                : (((EgeszSzam)szam).getPozitiv()?"+":"-") + tMivel.bol(tMit).toSzamString()
                );
    }

    @Override
    public EgeszSzam bol(Szam szam) {
        return null;
    }

    @Override
    public Szam szorozva(Szam szam) {
        return super.szorozva(szam);
    }

    @Override
    public Szam osztva(Szam szam) {
        return super.osztva(szam);
    }

    @Override
    public boolean nagyobb(Szam szam){
        tMivel.setEgesz( ((TermeszetesSzam)szam).getEgesz() );
        tMit.setEgesz( this.getEgesz() );
        return this.getPozitiv() ? ((EgeszSzam)szam).getPozitiv() && tMit.nagyobb(tMivel)
                                 : ((EgeszSzam)szam).getPozitiv() || tMit.kisebb(tMivel);
    }

    @Override
    public boolean kisebb(Szam szam) {
        tMivel.setEgesz( ((TermeszetesSzam)szam).getEgesz() );
        tMit.setEgesz( this.getEgesz() );
        return this.getPozitiv() ? !((EgeszSzam)szam).getPozitiv() || tMit.kisebb(tMivel)
                                 : !((EgeszSzam)szam).getPozitiv() && tMit.nagyobb(tMivel);
    }

    @Override
    public boolean egyenlo(Szam szam) {
        tMivel.setEgesz( ((TermeszetesSzam)szam).getEgesz() );
        tMit.setEgesz( this.getEgesz() );
        return tMit.egyenlo(tMivel) && ((EgeszSzam)szam).getPozitiv()==this.getPozitiv();
    }

    @Override
    public String toString() {
        return "x= "+ (this.pozitiv ? "+" : "-") +
                this.egesz.stream()
                        .map(Object::toString)
                        .collect(Collectors.joining());
    }

    @Override
    public String toSzamString() {
        return this.egesz.stream()
                .map(Object::toString)
                .collect(Collectors.joining());
    }
}
