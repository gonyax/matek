package SzamAbrazolas;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RacionalisSzam extends EgeszSzam implements Szam {
    /*
     *  A negatív és pozitív egész és nem végtelen tizedes törtszámok és a nulla
     */

    protected List<Byte> tort; // A tizedes érték a lista 1. (0) eleme

    public RacionalisSzam() { this.tort = null; }

    public RacionalisSzam(String rac) {
        super(Szam.wStringCreate(Szam.wStringEgesz(rac)));
        Integer commaPoz = Szam.wStringPoz(rac);
        //Szam.szamValidalas( !this.isTermeszetesSzam(wEgesz), "A szám nem racionális szám!");
        String tortresz = commaPoz>=0 ? rac.substring(commaPoz+1) : null;
        this.tort = tortresz==null
                        ? Arrays.asList(new Byte[] {0})
                        : tortresz.chars()
                            .mapToObj(x -> (byte)(x-48))
                            .collect(Collectors.toList());
    }

    public static boolean isRacionalisSzam(String szam) {
        return false;
    }

    public List<Byte> getTort() {
        return tort;
    }

    @Override
    public EgeszSzam meg(Szam szam) {
        return super.meg(szam);
    }

    @Override
    public EgeszSzam bol(Szam szam) {
        return super.bol(szam);
    }

    @Override
    public Szam szorozva(Szam szam) {
        return super.szorozva(szam);
    }

    @Override
    public Szam osztva(Szam szam) {
        return super.osztva(szam);
    }

    @Override
    public boolean nagyobb(Szam szam) {
        return super.nagyobb(szam);
    }

    @Override
    public boolean kisebb(Szam szam) {
        return super.kisebb(szam);
    }

    @Override
    public boolean egyenlo(Szam szam) {
        return super.egyenlo(szam);
    }

       @Override
    public String toSzamString() {
        return super.toSzamString();
    }

    @Override
    public String toString() {
        return "x= " + (this.pozitiv ? "+" : "-") +
                this.egesz.stream()
                        .map(Object::toString)      // .map(x -> x.toString()) helyett
                        .collect(Collectors.joining()) +
                "," +
                this.tort.stream()
                        .map(Object::toString)      // .map(x -> x.toString())
                        .collect(Collectors.joining());
    }
}
