package SzamAbrazolas;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class EgeszSzamTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testEgeszSzam() {
        EgeszSzam es = new EgeszSzam("123");
        assertThat(es.toString(), equalTo("x= +123"));
        System.out.println(es);

        es = new EgeszSzam("-4123");
        assertThat(es.toString(), equalTo("x= -4123"));
        System.out.println(es);
    }

    @Test
    public void testIsEgeszSzam() {
        assertThat(EgeszSzam.isEgeszSzam("123"), equalTo(true));
        assertThat(EgeszSzam.isEgeszSzam("+123"), equalTo(true));
        assertThat(EgeszSzam.isEgeszSzam("++123"), equalTo(false));
        assertThat(EgeszSzam.isEgeszSzam("-123"), equalTo(false));
        assertThat(EgeszSzam.isEgeszSzam("--123"), equalTo(false));
        assertThat(EgeszSzam.isEgeszSzam("-1g3"), equalTo(false));
        assertThat(EgeszSzam.isEgeszSzam("a123"), equalTo(false));
        assertThat(EgeszSzam.isEgeszSzam("-123#"), equalTo(false));
    }

    @Test
    public void testWithNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("A szám nem lehet null vagy üres");
        EgeszSzam egeszSzam = new EgeszSzam(null);
    }

    @Test
    public void testWithEmptyString() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("A szám nem lehet null vagy üres");
        EgeszSzam egeszSzam = new EgeszSzam("");
    }

    @Test
    public void testWithABC() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("A szám csak számjegyeket tartalmazhat");
        EgeszSzam egeszSzam  = new EgeszSzam("1234fgt1234");
    }

    @Test
    public void testWithDot() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("A szám csak számjegyeket tartalmazhat");
        EgeszSzam egeszSzam  = new EgeszSzam("1234.1234");
    }

    @Test
    public void testNagyobb() {
        EgeszSzam t = new EgeszSzam("1000");
        assertThat(t.nagyobb( new EgeszSzam("0") ), equalTo(false) );
        assertThat(t.nagyobb( new EgeszSzam("+100") ), equalTo(false) );
        assertThat(t.nagyobb( new EgeszSzam("-100") ), equalTo(false) );
        assertThat(t.nagyobb( new EgeszSzam("+999") ), equalTo(false) );
        assertThat(t.nagyobb( new EgeszSzam("+1000") ), equalTo(false) );
        assertThat(t.nagyobb( new EgeszSzam("1000") ), equalTo(false) );
        assertThat(t.nagyobb( new EgeszSzam("1001") ), equalTo(true) );
        assertThat(t.nagyobb( new EgeszSzam("-1001") ), equalTo(false) );
        assertThat(t.nagyobb( new EgeszSzam("100000") ), equalTo(true) );

        t = new EgeszSzam("-1000");
        assertThat(t.nagyobb( new EgeszSzam("0") ), equalTo(true) );
        assertThat(t.nagyobb( new EgeszSzam("-100") ), equalTo(true) );
        assertThat(t.nagyobb( new EgeszSzam("100") ), equalTo(true) );
        assertThat(t.nagyobb( new EgeszSzam("-999") ), equalTo(true) );
        assertThat(t.nagyobb( new EgeszSzam("-1000") ), equalTo(false) );
        assertThat(t.nagyobb( new EgeszSzam("1000") ), equalTo(true) );
        assertThat(t.nagyobb( new EgeszSzam("-1001") ), equalTo(false) );
        assertThat(t.nagyobb( new EgeszSzam("-100000") ), equalTo(false) );
    }

    @Test
    public void testEgyenlo() {
        EgeszSzam t = new EgeszSzam("1000");
        assertThat(t.egyenlo( new EgeszSzam("+100") ), equalTo(false) );
        assertThat(t.egyenlo( new EgeszSzam("+999") ), equalTo(false) );
        assertThat(t.egyenlo( new EgeszSzam("+1000") ), equalTo(true) );
        assertThat(t.egyenlo( new EgeszSzam("1000") ), equalTo(true) );
        assertThat(t.egyenlo( new EgeszSzam("1001") ), equalTo(false) );
        assertThat(t.egyenlo( new EgeszSzam("100000") ), equalTo(false) );

        t = new EgeszSzam("-1000");
        assertThat(t.egyenlo( new EgeszSzam("-100") ), equalTo(false) );
        assertThat(t.egyenlo( new EgeszSzam("+100") ), equalTo(false) );
        assertThat(t.egyenlo( new EgeszSzam("+1000") ), equalTo(false) );
        assertThat(t.egyenlo( new EgeszSzam("-1000") ), equalTo(true) );
        assertThat(t.egyenlo( new EgeszSzam("-1001") ), equalTo(false) );
        assertThat(t.egyenlo( new EgeszSzam("-100000") ), equalTo(false) );
    }

    @Test
    public void testKisebb() {
        EgeszSzam t = new EgeszSzam("1000");
        assertThat(t.kisebb( new EgeszSzam("+100") ), equalTo(true) );
        assertThat(t.kisebb( new EgeszSzam("-100") ), equalTo(true) );
        assertThat(t.kisebb( new EgeszSzam("+999") ), equalTo(true) );
        assertThat(t.kisebb( new EgeszSzam("+1000") ), equalTo(false) );
        assertThat(t.kisebb( new EgeszSzam("1000") ), equalTo(false) );
        assertThat(t.kisebb( new EgeszSzam("-1000") ), equalTo(true) );
        assertThat(t.kisebb( new EgeszSzam("100000") ), equalTo(false) );
        assertThat(t.kisebb( new EgeszSzam("-000999") ), equalTo(true) );

        t = new EgeszSzam("-1000");
        assertThat(t.kisebb( new EgeszSzam("+100") ), equalTo(false) );
        assertThat(t.kisebb( new EgeszSzam("-100") ), equalTo(false) );
        assertThat(t.kisebb( new EgeszSzam("+999") ), equalTo(false) );
        assertThat(t.kisebb( new EgeszSzam("+1000") ), equalTo(false) );
        assertThat(t.kisebb( new EgeszSzam("1000") ), equalTo(false) );
        assertThat(t.kisebb( new EgeszSzam("-1000") ), equalTo(false) );
        assertThat(t.kisebb( new EgeszSzam("-1001") ), equalTo(true) );
        assertThat(t.kisebb( new EgeszSzam("-100000") ), equalTo(true) );
        assertThat(t.kisebb( new EgeszSzam("-009999") ), equalTo(true) );
    }

    @Test
    public void testMeg() {
        EgeszSzam t = new EgeszSzam("934483");
        assertThat(t.meg(new EgeszSzam("11466520")).toString(), equalTo("x= +12401003"));
        assertThat(t.meg(new EgeszSzam("-11466520")).toString(), equalTo("x= -10532037"));
        assertThat(t.meg(new EgeszSzam("000001")).toString(), equalTo("x= +934484"));

        t = new EgeszSzam("-934483");
        assertThat(t.meg(new EgeszSzam("11466520")).toString(), equalTo("x= +10532037"));
        assertThat(t.meg(new EgeszSzam("-11466520")).toString(), equalTo("x= -12401003"));
        assertThat(t.meg(new EgeszSzam("000001")).toString(), equalTo("x= -934482"));
        assertThat(t.meg(new EgeszSzam("-000001")).toString(), equalTo("x= -934484"));

        EgeszSzam t2 = new EgeszSzam("00000934483");
        assertThat(t2.meg(new EgeszSzam("000001")).toString(), equalTo("x= +934484"));
        assertThat(t2.meg(new EgeszSzam("-000001")).toString(), equalTo("x= +934482"));
    }
}