package SzamAbrazolas;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class TermeszetesSzamTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testSzamtisztitas() {
        assertThat(new TermeszetesSzam("0000").szamTisztitas().toString(), equalTo("x= 0"));
        assertThat(new TermeszetesSzam("0").szamTisztitas().toString(), equalTo("x= 0"));
        assertThat(new TermeszetesSzam("0001").szamTisztitas().toString(), equalTo("x= 1"));
        assertThat(new TermeszetesSzam("0120").szamTisztitas().toString(), equalTo("x= 120"));
        assertThat(new TermeszetesSzam("+0120").szamTisztitas().toString(), equalTo("x= 120"));
        assertThat(new TermeszetesSzam("+000").szamTisztitas().toString(), equalTo("x= 0"));
    }

    @Test
    public void testMeg() {
        TermeszetesSzam t = new TermeszetesSzam("934483");
        assertThat(t.meg(new TermeszetesSzam("11466520")).toString(), equalTo("x= 12401003"));
        assertThat(t.meg(new TermeszetesSzam("000001")).toString(), equalTo("x= 934484"));

        TermeszetesSzam t2 = new TermeszetesSzam("00000934483");
        assertThat(t2.meg(new TermeszetesSzam("000001")).toString(), equalTo("x= 934484"));

        TermeszetesSzam t3 = new TermeszetesSzam("192290");
        assertThat(t3.meg(new TermeszetesSzam("820000")).toString(), equalTo("x= 1012290"));
    }

    @Test
    public void testBol_Neg() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Hibás kivonás");
        TermeszetesSzam t = new TermeszetesSzam("934483");
        TermeszetesSzam diff = t.bol(new TermeszetesSzam("+3000000"));
    }

    @Test
    public void testBol() {
        TermeszetesSzam t = new TermeszetesSzam("934483");
        assertThat( t.bol( new TermeszetesSzam("3")).toString(), equalTo("x= 934480"));
        assertThat( t.bol( new TermeszetesSzam("483")).toString(), equalTo("x= 934000"));
        assertThat( t.bol( new TermeszetesSzam("934482")).toString(), equalTo("x= 1"));
        assertThat( t.bol( new TermeszetesSzam("00000934482")).toString(), equalTo("x= 1"));

        t = new TermeszetesSzam("11466520");
        assertThat(t.bol(new TermeszetesSzam("934483")).toString(), equalTo("x= 10532037"));
    }

    @Test
    public void testSzorozva() {
        TermeszetesSzam t = new TermeszetesSzam("12345");
        assertThat( t.szorozva( new TermeszetesSzam("8")).toString(), equalTo("x= 98760"));
        assertThat( t.szorozva( new TermeszetesSzam("82")).toString(), equalTo("x= 1012290"));
    }

    @Test
    public void testWithNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("A szám nem lehet null vagy üres");
        TermeszetesSzam termeszetesSzam = new TermeszetesSzam(null);
    }

    @Test
    public void testWithEmptyString() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("A szám nem lehet null vagy üres");
        TermeszetesSzam termeszetesSzam = new TermeszetesSzam("");
    }

    @Test
    public void testWithABC() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("A szám csak számjegyeket tartalmazhat");
        TermeszetesSzam termeszetesSzam = new TermeszetesSzam("1234fgt1234");
    }

    @Test
    public void testIsTermeszetesSzam() {
        assertThat(TermeszetesSzam.isTermeszetesSzam("12454646310"), equalTo(true));
        assertThat(TermeszetesSzam.isTermeszetesSzam("-12454646310"), equalTo(false));
        assertThat(TermeszetesSzam.isTermeszetesSzam("1245abc4646310"), equalTo(false));
        assertThat(TermeszetesSzam.isTermeszetesSzam("+124510"), equalTo(true));
        assertThat(TermeszetesSzam.isTermeszetesSzam(""), equalTo(false));
        assertThat(TermeszetesSzam.isTermeszetesSzam(null), equalTo(false));
    }

    @Test
    public void testKisebb() {
        TermeszetesSzam t = new TermeszetesSzam("1000");
        assertThat(t.kisebb( new TermeszetesSzam("+100") ), equalTo(true) );
        assertThat(t.kisebb( new TermeszetesSzam("+999") ), equalTo(true) );
        assertThat(t.kisebb( new TermeszetesSzam("+1000") ), equalTo(false) );
        assertThat(t.kisebb( new TermeszetesSzam("1000") ), equalTo(false) );
        assertThat(t.kisebb( new TermeszetesSzam("1001") ), equalTo(false) );
        assertThat(t.kisebb( new TermeszetesSzam("100000") ), equalTo(false) );

        assertThat(t.kisebb( new TermeszetesSzam("+000999") ), equalTo(true) );
    }

    @Test
    public void testEgyenlo() {
        TermeszetesSzam t = new TermeszetesSzam("1000");
        assertThat(t.egyenlo( new TermeszetesSzam("+100") ), equalTo(false) );
        assertThat(t.egyenlo( new TermeszetesSzam("+999") ), equalTo(false) );
        assertThat(t.egyenlo( new TermeszetesSzam("+1000") ), equalTo(true) );
        assertThat(t.egyenlo( new TermeszetesSzam("1000") ), equalTo(true) );
        assertThat(t.egyenlo( new TermeszetesSzam("1001") ), equalTo(false) );
        assertThat(t.egyenlo( new TermeszetesSzam("100000") ), equalTo(false) );
    }

    @Test
    public void testNagyobb() {
        TermeszetesSzam t = new TermeszetesSzam("1000");
        assertThat(t.nagyobb( new TermeszetesSzam("+100") ), equalTo(false) );
        assertThat(t.nagyobb( new TermeszetesSzam("+999") ), equalTo(false) );
        assertThat(t.nagyobb( new TermeszetesSzam("+1000") ), equalTo(false) );
        assertThat(t.nagyobb( new TermeszetesSzam("1000") ), equalTo(false) );
        assertThat(t.nagyobb( new TermeszetesSzam("1001") ), equalTo(true) );
        assertThat(t.nagyobb( new TermeszetesSzam("100000") ), equalTo(true) );
    }
}