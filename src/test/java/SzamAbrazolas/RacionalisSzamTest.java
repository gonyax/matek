package SzamAbrazolas;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class RacionalisSzamTest {

    @Test
    public void testConstructorRacionalisSzam() {
        assertThat( new RacionalisSzam("-12345,123123123").toString(), equalTo("x= -12345,123123123"));
        assertThat( new RacionalisSzam("0,123123123").toString(), equalTo("x= +0,123123123"));
        assertThat( new RacionalisSzam("-0,123123123").toString(), equalTo("x= -0,123123123"));
        assertThat( new RacionalisSzam("3123").toString(), equalTo("x= +3123,0"));
        assertThat( new RacionalisSzam("-3123").toString(), equalTo("x= -3123,0"));
        assertThat( new RacionalisSzam("31.23").toString(), equalTo("x= +31,23"));
    }
}